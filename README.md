# venturx-app

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## To Run the Project

* Make sure that you've already installed the Node.
* Open you cmd/terminal with administrative permissions and Run `npm install -g grunt-cli bower`.
* Then `cd` into Project directory.
* Inside Project dir Run `npm install`.
* Then Run `bower install`.
* Finally Run `grunt server`.

## Output

![picture](app/images/output.png)
