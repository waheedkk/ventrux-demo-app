'use strict';

/**
 * @ngdoc overview
 * @name venturX_App
 * @description
 * # venturX_App
 *
 * Main module of the application.
 */
angular
  .module('venturX_App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.knob'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
