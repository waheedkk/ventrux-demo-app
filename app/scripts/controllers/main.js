'use strict';

/**
 * @ngdoc function
 * @name venturX_App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the venturX_App
 */
angular.module('venturX_App')
  .controller('MainCtrl', function ($scope) {
    $scope.charts = [];
    $scope.charts.push(drawChart(27, 'rgba(251, 168, 45, 1)', '%', 'Product'));
    $scope.charts.push(drawChart(3, 'rgba(245, 5, 5, 1)', 'months', 'Runway', 12));
    $scope.charts.push(drawChart(24, 'rgba(24, 94, 205, 0.92)', '%', 'Conversion'));
    $scope.charts.push(drawChart(100, 'rgba(69, 205, 24, 0.92)', '%', 'Engagement'));

    function drawChart (_value, _barColor, _unit, _text, _maxVal) {
      return {
        value: _value,
        options: {
          size: 160,
          readOnly: true,
          displayPrevious: true,
          barCap: 25,
          trackWidth: 8,
          barWidth: 8,
          unit: _unit,
          barColor: _barColor,
          trackColor: 'rgba(30, 18, 1, 0.08)',
          textColor: 'black',
          subText: {
            enabled: true,
            text: _text,
            color: 'black'
          },
          max: _maxVal ? _maxVal : 100
        }
      }
    }
  });
